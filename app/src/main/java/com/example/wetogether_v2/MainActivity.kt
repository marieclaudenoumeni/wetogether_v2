package com.example.wetogether_v2

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tabPagerAdapter = TabPagerAdapter(supportFragmentManager, 5)


        val viewPager: ViewPager = findViewById(R.id.pager)
        viewPager.adapter = tabPagerAdapter

        val tabs: TabLayout = findViewById(R.id.tab_layout)
        tabs.setupWithViewPager(viewPager)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        //setSupportActionBar(toolbar as Toolbar?)

        configureTabLayout()

        setupTabLayout(tabs)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main,  menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                Toast.makeText(applicationContext, "click on setting", Toast.LENGTH_LONG).show()
                true
            }
            R.id.action_search ->{
                Toast.makeText(applicationContext, "click on search", Toast.LENGTH_LONG).show()
                return true
            }
            R.id.action_exit ->{
                Toast.makeText(applicationContext, "click on exit", Toast.LENGTH_LONG).show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun setupTabLayout(tabLayout: TabLayout) {
        //tabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        // tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //tabLayout.setupWithViewPager(mViewpager)

        tabLayout.getTabAt(0)?.setIcon(R.drawable.ic_home_black_24dp)
        tabLayout.getTabAt(0)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(1)?.setIcon(R.drawable.ic_fingerprint_black_24dp)
        tabLayout.getTabAt(1)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(2)?.setIcon(R.drawable.ic_share_black_24dp)
        tabLayout.getTabAt(2)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(3)?.setIcon(R.drawable.ic_chat_black_24dp)
        tabLayout.getTabAt(3)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

        tabLayout.getTabAt(4)?.setIcon(R.drawable.ic_person_black_24dp)
        tabLayout.getTabAt(4)?.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED

    }


    private fun configureTabLayout() {

       tab_layout.addTab(tab_layout.newTab().setText("Tab 1 Item"))
        tab_layout.addTab(tab_layout.newTab().setText("Tab 2 Item"))
        tab_layout.addTab(tab_layout.newTab().setText("Tab 3 Item"))
        tab_layout.addTab(tab_layout.newTab().setText("Tab 4 Item"))
        tab_layout.addTab(tab_layout.newTab().setText("Tab 5 Item"))

        val adapter = TabPagerAdapter(supportFragmentManager, 5)

        val pager: ViewPager = findViewById(R.id.pager)
        pager.adapter = adapter

        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab_layout))
        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
    }

}
