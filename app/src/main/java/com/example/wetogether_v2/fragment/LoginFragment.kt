package com.example.wetogether_v2.fragment

import android.app.Activity
import android.os.Bundle
import android.provider.Settings.Global.getString
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar

import com.example.wetogether_v2.R
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.annotation.StringRes


import com.example.wetogether_v2.database.DBHelper
import com.example.wetogether_v2.database.MindOrksDBOpenHelper
import com.example.wetogether_v2.database.UserModel
import com.example.wetogether_v2.ui.login.LoggedInUserView
import com.example.wetogether_v2.ui.login.LoginViewModel
import com.example.wetogether_v2.ui.login.LoginViewModelFactory


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

private lateinit var databaseHelper: DBHelper
private lateinit var loginViewModel: LoginViewModel

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       // return inflater.inflate(R.layout.fragment_login, container, false)

        var view = inflater.inflate(R.layout.fragment_login, container, false)

        val username = view?.findViewById<EditText>(R.id.username)
        val password = view?.findViewById<EditText>(R.id.password)
        val login = view?.findViewById<Button>(R.id.login)
        val loading = view?.findViewById<ProgressBar>(R.id.loading)

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        // val view = inflater.inflate(R.layout.fragment_login, container, false)

        loginViewModel.loginFormState.observe(viewLifecycleOwner, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            if (login != null) {
                login.isEnabled = loginState.isDataValid
            }

            if (loginState.usernameError != null) {
                if (username != null) {
                    username.error = getString(loginState.usernameError)
                }
            }
            if (loginState.passwordError != null) {
                if (password != null) {
                    password.error = getString(loginState.passwordError)
                }
            }
        })

        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {
            val loginResult = it ?: return@Observer

            if (loading != null) {
                loading.visibility = View.GONE
            }
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }
            activity?.setResult((Activity.RESULT_OK))
            //getActivity().setResult(Activity.RESULT_OK, data)
            //  setResult(Activity.RESULT_OK)

            //Complete and destroy login activity once successful
            //finish()
            activity?.finish()

        })

        username?.afterTextChanged {
            if (password != null) {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }
        }

        password?.apply {
            afterTextChanged {
                if (username != null) {
                    loginViewModel.loginDataChanged(
                        username.text.toString(),
                        password.text.toString()
                    )
                }
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        if (username != null) {
                            loginViewModel.login(
                                username.text.toString(),
                                password.text.toString()
                            )
                        }
                }
                false
            }

            login?.setOnClickListener {
                if (loading != null) {
                    loading.visibility = View.VISIBLE
                }
                if (username != null) {
                    loginViewModel.login(username.text.toString(), password.text.toString())
                }

                val dbHandler = MindOrksDBOpenHelper(context, null)

                /* var result =   databaseHelper.insertUser(
                     UserModel(
                         email = username?.text.toString(),
                         pass = password.text.toString()
                     )
                 )*/

                /*  val cursor = dbHandler.getAllName()
                  cursor!!.moveToFirst()
                  username?.append((cursor.getString(cursor.getColumnIndex(MindOrksDBOpenHelper.COLUMN_NAME))))
                  while (cursor.moveToNext()) {
                      username?.append((cursor.getString(cursor.getColumnIndex(MindOrksDBOpenHelper.COLUMN_NAME))))
                      username?.append("\n")
                  }
                  cursor.close()*/

            }
        }

        /*if (username != null) {
            if (password != null) {
                UserModel(
                    email = username.text.toString().trim(),
                    pass = password.text.toString().trim()
                )
            }
        }*/


        /* if (username != null) {
             if (!databaseHelper.checkUser(username.text.toString().trim())) {
                 // val dbHandler = context?.let { MindOrksDBOpenHelper(it, null) }
                 var result = databaseHelper.insertUser(
                     UserModel(
                         pass = password?.text.toString().trim(),
                         email = username?.text.toString().trim()
                     )
                 )
             }
         }*/
        return view

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LoginFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LoginFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


private fun updateUiWithUser(model: LoggedInUserView) {
    val welcome = getString(R.string.welcome)
    val displayName = model.displayName
    // TODO : initiate successful logged in experience
    Toast.makeText(context, "$welcome $displayName", Toast.LENGTH_LONG).show()
}

private fun showLoginFailed(@StringRes errorString: Int) {
    //Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show()
    Toast.makeText(context, errorString, Toast.LENGTH_SHORT).show()
    // Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
}

}


/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

