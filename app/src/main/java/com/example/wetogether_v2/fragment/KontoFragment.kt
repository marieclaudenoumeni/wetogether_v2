package com.example.wetogether_v2.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast

import com.example.wetogether_v2.R
import com.example.wetogether_v2.data.model.LoginRepository

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [KontoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class KontoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_konto, container, false)


        var buttonlogout = view?.findViewById<Button>(R.id.logout)
        var buttonprofil = view?.findViewById<Button>(R.id.MeinProfil)
        var buttonhilfe = view?.findViewById<Button>(R.id.hilfe)
        var buttondropdown = view?.findViewById<Button>(R.id.dropdown)

        // when button is clicked, show the alert
        buttonlogout?.setOnClickListener {
            // build alert dialog
            val dialogBuilder = AlertDialog.Builder(activity)

            // set message of alert dialog
            dialogBuilder.setMessage("Do you want sign out?")
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("Proceed", DialogInterface.OnClickListener { dialog, id -> activity?.finish()
                })
                // negative button text and action
                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id -> dialog.cancel()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("AlertDialogExample")
            // show alert dialog
            alert.show()
        }
        buttonlogout?.setOnClickListener(View.OnClickListener {
            fun Click(loginRepository: LoginRepository) {
                loginRepository.logout()
            }
            /*val dataSource: LoginDataSource = object: LoginDataSource
            val loginRepository: LoginRepository = object: LoginRepository(dataSource)
            loginRepository(dataSource).logout()*/
        })

        buttonhilfe?.setOnClickListener(View.OnClickListener {
            Toast.makeText(context,  "This application help you to find and communicate with cooking friend", Toast.LENGTH_LONG).show()
        })

        buttonprofil?.setOnClickListener(View.OnClickListener {
            Toast.makeText(context,  "you are added an accounts", Toast.LENGTH_LONG).show()
        })


        var spinner = view?.findViewById<Spinner>(R.id.spinner)
        // Create an ArrayAdapter
        val adapter = context?.let { ArrayAdapter.createFromResource(it, R.array.Sprachen, android.R.layout.simple_spinner_item) }
        // Specify the layout to use when the list of choices appears
        adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        if (spinner != null) {
            spinner.adapter = adapter
        }

        buttondropdown?.setOnClickListener(View.OnClickListener {
            if (spinner != null) {
                Toast.makeText(context, "Spinner 1 " + spinner.selectedItem.toString(), Toast.LENGTH_LONG).show()
            }

        })
        return view

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment KontoFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            KontoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
