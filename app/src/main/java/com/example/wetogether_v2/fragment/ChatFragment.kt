package com.example.wetogether_v2.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wetogether_v2.chat.*
import com.example.wetogether_v2.R


import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_chat.*
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

var fuser: FirebaseUser? = null
var reference: DatabaseReference? = null

var messageAdapter: MessageAdapter? = null
lateinit var mChat: MutableList<Chat>

lateinit var seenListener: ValueEventListener

lateinit var userId: String

lateinit var mUsers: ArrayList<User>
lateinit var usersList: ArrayList<Chatlist>
lateinit var userAdapter: UserAdapter


/**
 * A simple [Fragment] subclass.
 * Use the [ChatFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChatFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_chat, container, false)

        var view=  inflater.inflate(R.layout.fragment_chat, container, false)

        /* setSupportActionBar(toolbar)
            supportActionBar?.title = ""
            supportActionBar?.setDisplayHomeAsUpEnabled(true)*/

        // setup the view
        // recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = LinearLayoutManager(context)

        // get users
        fuser = FirebaseAuth.getInstance().currentUser
        usersList = arrayListOf()
        //userId = intent.getStringExtra("USER_ID")

        btn_send.setOnClickListener {
            val msg = text_send.text.toString()
            if (msg != ""){
                sendMessage(fuser?.uid, userId, msg)
            } else {
                Toast.makeText(context, "You can't send empty message", Toast.LENGTH_SHORT).show()
            }
            // send a message
            text_send.setText("")
        }

        // firebase
        reference = FirebaseDatabase.getInstance().getReference("Users").child(userId)
        reference!!.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(e: DatabaseError) {
                Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                usersList.clear()
                // put chats data to variable
                for (snapshot in dataSnapshot.children){
                    val chatlist = snapshot.getValue(Chatlist::class.java)
                    usersList.add(chatlist!!)
                }
                chatList()
            }
        })

        seenMessage(userId)
        return view

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChatFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ChatFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }



    // get data chats
    private fun chatList(){
        mUsers = arrayListOf()
        reference = FirebaseDatabase.getInstance().getReference("Users")
        reference!!.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, databaseError.message, Toast.LENGTH_LONG).show()
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                mUsers.clear()
                // ambil data semua chat
                for (snapshot in dataSnapshot.children){
                    val user = snapshot.getValue(User::class.java)

                    // mencari data chat yang ada uid current user
                    for (chatlist in usersList){
                        if (user?.id.equals(chatlist.id)){
                            mUsers.add(user!!)
                        }
                    }

                }
                userAdapter = UserAdapter(context!!, mUsers, true)
                recycler_view.adapter = userAdapter
            }

        })
    }


    private fun seenMessage(useId: String?) {
        reference = FirebaseDatabase.getInstance().getReference("Chats")
        seenListener = reference!!.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(context, p0.message, Toast.LENGTH_LONG).show()

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot: DataSnapshot in dataSnapshot.children){
                    val chat = snapshot.getValue(Chat::class.java)
                    if (chat?.receiver.equals(fuser?.uid) && chat?.sender.equals(useId)){
                        val hashMap: HashMap<String, Any> = hashMapOf()
                        hashMap.put("isseen", true)
                        snapshot.ref.updateChildren(hashMap)
                    }
                }
            }

        })
    }

    private fun readMessages(myId: String?, userId: String?, imageURL: String?) {
        mChat = arrayListOf()

        reference = FirebaseDatabase.getInstance().getReference("Chats")
        reference!!.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(context, p0.message, Toast.LENGTH_LONG).show()
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                mChat.clear()

                for (snapshot: DataSnapshot in dataSnapshot.children){
                    val chat = snapshot.getValue(Chat::class.java)
                    if (chat?.receiver.equals(fuser?.uid) && chat?.sender.equals(userId) || chat?.receiver.equals(userId) && chat?.sender.equals(myId)){
                        mChat.add(chat!!)
                    }
                }
                // adapter
                messageAdapter = context?.let { MessageAdapter(it, mChat, imageURL!!) }
                recycler_view.adapter = messageAdapter
            }

        })
    }


    private fun sendMessage(sender: String?, receiver: String?, msg: String) {

        var reference = FirebaseDatabase.getInstance().reference

        val hashMap: HashMap<String, Any> = hashMapOf()
        hashMap.put("nama_key", "ISI DATA")

        reference.child("Chats").push().setValue(hashMap)

        // add user to chat fragment
        val chatRef = FirebaseDatabase.getInstance().getReference("Chatlist")
            .child(fuser?.uid!!)
            .child(userId)

        chatRef.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()){
                    chatRef.child("id").setValue(userId)
                }
            }

        })

        val chatRefReceiver = FirebaseDatabase.getInstance().getReference("Chatlist")
            .child(userId)
            .child(fuser?.uid!!)
        chatRefReceiver.child("id").setValue(fuser?.uid!!)

//        // for notification use
//        reference = FirebaseDatabase.getInstance().getReference("Chatlist").child(fuser?.uid!!)
//        reference.addListenerForSingleValueEvent(object : ValueEventListener{
//            override fun onCancelled(p0: DatabaseError) {
//
//            }
//
//            override fun onDataChange(dataSnapshot: DataSnapshot) {
//                val user = dataSnapshot.getValue(User::class.java)
//                // fot notification
//            }
//
//        })
    }

/*    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId){
            android.R.id.home -> {
                activity?.finish()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)

            }
        }
    }*/


}




