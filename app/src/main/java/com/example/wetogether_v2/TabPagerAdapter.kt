package com.example.wetogether_v2

/*import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter*/
//import androidx.fragment.app.Fragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentManager
import com.example.wetogether_v2.fragment.*

class TabPagerAdapter(fm: FragmentManager, private var tabCount: Int) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        when (position) {
            0 -> return HomeFragment()
            1 -> return LoginFragment()
            2 -> return ShareFragment()
            3 -> return ChatFragment()
            else -> return KontoFragment()
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}