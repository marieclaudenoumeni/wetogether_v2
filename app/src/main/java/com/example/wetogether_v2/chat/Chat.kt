package com.example.wetogether_v2.chat

data class Chat(var sender: String? = null,
                var receiver: String? = null,
                var message: String? = null,
                var isseen: Boolean? = null)