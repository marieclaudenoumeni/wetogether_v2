package com.example.wetogether_v2.chat

data class User(
    val id: String = "",
    val username: String = "",
    val imageURL: String = "",
    val status: String = "",
    var searsh: String = "")